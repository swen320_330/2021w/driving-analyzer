# Driving Analyzer

Driving Analyzer

# Development requirement

## Language requirements
- Python - Carloop interaction with website
-  Mysql - Database structure
- HTML&CSS - Web design

## Platfrom requirement
- System is web based(website)
- Web server on AWS EC2
- Uses Amazon RDS database on EC2 instance
- flask app for web communication and user authentication
- React framework for more systematic structuring of website


# Functional requirements

## requirement1
Capability Requirement: Data displayed on website
Description: Carloop data is stored on the website and displayed on the correct fields on the website as output.
Input(s): carloop data
Output(s): website
## requirement2
Capability Requirement: Calculated data displayed on website
Description: Miles per gallon is Calculated then displayed on website
Input(s): raw data
Output(s): Calculated data
## requirement3
Capability Requirement: User can interact with website
Description: User can input oil change date and website tracks when next oil change is due
Input(s): Oil change data
Output(s): Display next data for Oil change

# Interface requirements

## requirement1
User Interface Requirement
Interface Requirement: Input fields
Description: User can input information to interaction with website

# Non Functional requirements
### performance
Web server is seperated from Database server to improve performance
### security
User's personal information is encrypted
Only specified users can access their informations
### Availability
Web server is seperated from Database server to prevent failures
### Usability
Website is well structured and easy to use
Website is easily readable for users
Website service only runs on desktop devices. (no mobile app)
### Other Quality Attributes
Service is only scaled for one user for this project


# Deployment Guide
User plugs carloop into OBD2 port in the car and collects data after a driving session. Data is automatically sent to RDS database and automatically updated on website. User accesses website and types in carloop identifier and views drive data and driving instructions.
